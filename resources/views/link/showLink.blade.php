@extends('../layout');

@section('title')
    ShareItBaby.io - one link
@endsection

@section('content')
    <p><a href="{{ $link->link }}">{{ $link->name }}</a> {{ $link->description }}</p>
@endsection