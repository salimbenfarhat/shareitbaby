@extends('../layout');

@section('title')
    ShareItBaby.io - list of links
@endsection

@section('content')
    @foreach($links as $link)
        <p>
            <a href="{{ $link->link }}">{{ $link->name }}</a>
            - {{ $link->description }} -
            <a href="{{ route('showLink', ['slug' => $link->slug]) }}">Details</a>
            -
            <a href="{{ route('updateLink', ['slug' => $link->slug]) }}">Update</a>
             -
            <a href="{{ route('deleteLink', ['slug' => $link->slug]) }}">Remove</a>
        </p>
    @endforeach
@endsection