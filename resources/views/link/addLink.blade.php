@extends('../layout');

@section('title')
    ShareItBaby.io - {{ isset($link->slug) ? 'update' : 'create' }} a link
@endsection

@section('content')
    <form method="post" action="{{ isset($link->slug) ? route('updateLink', ['slug' => $link->slug]) : route('createLink') }}">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" placeholder="Set a name" class="form-control" value="{{ $link->name or '' }}">
        </div>
        <div class="form-group">
            <label for="link">Link</label>
            <input type="url" name="link" id="link" placeholder="Give the link" class="form-control" value="{{ $link->link or '' }}">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" id="description" class="form-control">{{ $link->description or '' }}</textarea>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <button type="submit" class="btn btn-info">{{ isset($link->slug) ? 'update' : 'create' }}</button>
        </div>
    </form>
@endsection