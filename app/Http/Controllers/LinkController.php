<?php

namespace shareitbaby\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use shareitbaby\Links;

class LinkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function listLink()
    {
        $links = Links::all();
        return view('link/listLink')->with('links', $links);
    }

    public function showLink($slug)
    {
        $link = Links::where('slug', '=', $slug)->first();
        return view('link/showLink')->with('link', $link);
    }

    public function deleteLink($slug)
    {
        $link = Links::where('slug', '=', $slug)->first();
        $link->delete();
        return redirect()->route('listLink')->with('success', 'Item was removed.');
    }
    
    public function updateLink(Request $request, $slug)
    {
        $link = Links::where('slug', '=', $slug)->first();
        if ($request->isMethod('post')) {
            $parameters = $request->except(['_token']);
            $date = new \DateTime(null);
            $link->name = $parameters['name'];
            $link->link = $parameters['link'];
            $link->description = $parameters['description'];
            $link->slug = Str::slug($parameters['name'] . $date->format('dmYhis'));
            $link->save();
            return redirect()->route('listLink')->with('success', 'Item was updated.');
        }
        return view('link/addLink')->with('link', $link);
    }
    
    public function addLink()
    {
        return View('link/addLink');
    }
    
    public function createLink(Request $request)
    {
        $parameters = $request->except(['_token']);
        //Links::create($parameters);
        $link = new Links();
        $date = new \DateTime(null);
        $link->name = $parameters['name'];
        $link->link = $parameters['link'];
        $link->description = $parameters['description'];
        $link->slug = Str::slug($parameters['name'] . $date->format('dmYhis'));
        $link->save();
        return redirect()->route('listLink')->with('success', 'Item was added.');
    }
    
    public function allJson(Request $request)
    {
        $collection = [
            ['name' => 'toto'],
            ['name' => 'tata'],
            ['name' => 'titi'],
        ];
        return response()->json($collection)->setCallback($request->input('callback'));
    }
}