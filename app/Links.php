<?php

namespace shareitbaby;

use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    protected $fillable = ['name', 'link', 'description'];
}
